
import cocos, pyglet, math
import keyboard
from pyglet.gl import *
from pyglet.window import key

def get_gravity():
    return (0.0, -100.0)

class player(cocos.cocosnode.CocosNode):
    def __init__(self, controller, position, rotation=-90):
        super(player, self).__init__()
        self.position = position
        self.velocity = (0, 0)
        self.rotation = rotation
        self.thrust = 0
        self.controller = controller
        self.schedule(self.update)

    def update(self, dt):
        g = get_gravity()
        vel_x = self.velocity[0]
        vel_y = self.velocity[1]

        vel_x += (g[0] + self.thrust * math.cos(self.rotation * math.pi / 180.0) - vel_x * 0.05) * dt
        vel_y += (g[1] - self.thrust * math.sin(self.rotation * math.pi / 180.0) - vel_y * 0.05) * dt


        self.velocity = (vel_x, vel_y)
        self.position = (self.position[0] + vel_x * dt, self.position[1] + vel_y * dt)
        self.controller.update(self, dt)

    def draw(self):
        glPushMatrix()
        self.transform()

        glBegin(GL_TRIANGLES)

        glColor3f(1, 1, 1)
        glVertex2f(10, 0)
        glVertex2f(-10, 7)
        glVertex2f(-10, -7)

        glEnd()
        glPopMatrix()


class player_controller:
    def update(self, obj, dt):
        turn = 0.0
        if keyboard.is_key_down(key.A):
            turn -= 1
        if keyboard.is_key_down(key.D):
            turn += 1
        obj.rotation += turn * 180 * dt

        obj.thrust = 0.0
        if keyboard.is_key_down(key.W):
            obj.thrust = 250.0
