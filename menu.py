import cocos
from player import *
from keyboard import *

class main_menu(cocos.menu.Menu):
    def __init__(self):
        super(main_menu, self).__init__()

        l = []
        l.append(cocos.menu.MenuItem('Play', self.on_new_game))
        l.append(cocos.menu.MenuItem('Quit', self.on_quit))
        self.create_menu(l)

    def on_new_game(self):
        print("on_new_game")

    def on_quit(self):
        quit()


class main_menu_layer(cocos.layer.Layer):
    def __init__(self):
        super(main_menu_layer, self).__init__()
        frame = cocos.director.director.get_window_size()

        label = cocos.text.Label(
            'Flyer',
            font_name = 'Times New Roman',
            font_size = 32,
            anchor_x = 'center', anchor_y = 'center'
            )
        label.position = frame[0] // 2, frame[1] - 50

        self.add(label)
        self.add(main_menu())
        self.add(keyboard_input())

        self.ship = player(player_controller(), (100, 100))
        self.add(self.ship)
