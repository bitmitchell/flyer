
import cocos

keys = {}

def is_key_down(key):
    if not key in keys:
        return False
    return keys[key]


class keyboard_input(cocos.layer.Layer):
    is_event_handler = True

    def __init__(self):
        super(keyboard_input, self).__init__()

    def on_key_press(self, key, mod):
        #print("Key down", key)
        keys[key] = True

    def on_key_release(self, key, mod):
        #print("Key up", key)
        keys[key] = False
